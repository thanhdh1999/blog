<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostsController;
Route::group(['prefix' => 'posts'], function () {
    Route::get('/', [PostsController::class, 'index'])->name('posts.index');
    Route::get('/create', [PostsController::class, 'create'])->name('posts.create');
    Route::post('/store', [PostsController::class, 'store'])->name('posts.store');
    Route::get('/edit/{id}', [PostsController::class, 'edit'])->name('posts.edit');
    Route::post('/update/{id}', [PostsController::class, 'update'])->name('posts.update');
    Route::delete('/destroy/{id}', [PostsController::class, 'destroy'])->name('posts.destroy');
});
