<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CommentsController;

Route::group(['prefix' => 'comments'], function () {
    Route::get('/', [CommentsController::class, 'index'])->name('comments.index');
    Route::get('/create', [CommentsController::class, 'create'])->name('comments.create');
    Route::post('/store', [CommentsController::class, 'store'])->name('comments.store');
    Route::post('/update/{id}', [CommentsController::class, 'update'])->name('comments.update');
    Route::get('/destroy/{id}', [CommentsController::class, 'destroy'])->name('comments.destroy');
});
