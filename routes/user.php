<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsersController;

Route::group(['prefix' => 'users'], function () {
    Route::get('/', [UsersController::class, 'index'])->name('users.index');
    Route::get('/create', [UsersController::class, 'create'])->name('users.create');
    Route::post('/store', [UsersController::class, 'store'])->name('users.store');
    Route::get('/edit/{id}', [UsersController::class, 'edit'])->name('users.edit');
    Route::post('/update/{id}', [UsersController::class, 'update'])->name('users.update');
    Route::get('/destroy/{id}', [UsersController::class, 'destroy'])->name('users.destroy');
    Route::get('/detail/{id}', [UsersController::class, 'detail'])->name('users.detail');
});
