@extends('layouts.master')

@section('title')
    <title>Danh sách comment</title>

@endsection
@section('content')
    <table class="table">
        <thead>
        <tr>
            <th scope="col">STT</th>
            <th scope="col">Tên</th>
            <th scope="col">Email</th>
            <th scope="col">Ảnh</th>
            <th scope="col">Ngày đăng ký</th>

        </tr>
        </thead>
        <tbody>
        @foreach($users as $i => $user)
            <tr>
                <th scope="row">{{$i +1 }}</th>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->image}}</td>
                <td>{{$user->created_at}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
