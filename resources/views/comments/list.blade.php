@extends('layouts.master')

@section('title')
    <title>Comment</title>

@endsection
@section('content')
    <table class="table">
        <thead>
        <tr>
            <th scope="col">STT</th>
            <th scope="col">Nội dung</th>
            <th scope="col">Người bình luận</th>
            <th scope="col">Thuộc bài viết</th>
            <th scope="col">Thời gian bình luận</th>
            <th scope="col">Trạng thái</th>
        </tr>
        </thead>
        <tbody>
        @foreach($comments as $i => $comment)
            <tr>
                <th scope="row">{{$i +1 }}</th>
                <td>{{$comment->content}}</td>
                <td>{{optional($comment->user)->name }}</td>
                <td>{{optional($comment->post)->title }}</td>
                <td>{{$comment->created_at}}</td>
                <td>
                    <button class="{{$comment->status == 0 ? 'btn btn-warning' : 'btn btn-success'}}" onclick="activeComment({{$comment->id}})">
                        {{$comment->status == 0 ? 'Inactive' : 'Active'}}
                    </button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
