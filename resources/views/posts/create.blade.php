@extends('layouts.master')

@section('title')
    <title>thêm bài post</title>
@endsection
@section('content')
    <form action="{{route('posts.store')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="mb-3">
            <label class="form-label">Tiêu đề</label>
            <input type="text" class="form-control" name="title">
        </div>
        <div class="mb-3">
            <label class="form-label">Nội dung</label>
            <textarea type="text" class="form-control" rows="5" cols="10" name="content" ></textarea>
        </div>
        <div class="mb-3">
            <label class="form-label">Mô tả</label>
            <textarea type="text" class="form-control" rows="5" cols="10" name="description"></textarea>
        </div>
        <div class="mb-3">
            <label class="form-label">Trạng thái</label>
            <input type="number" class="form-control" rows="5" cols="10" value="0" name="status">
        </div>
        <div class="mb-3">
            <label class="form-label">Chọn ảnh</label>
            <div id="posts_image_upload" class="dropzone"></div>
        </div>

        <button type="submit" class="btn btn-primary bg-primary">Lưu</button>
        <a href="{{ route('posts.index') }}" class="btn btn-success">Danh sách</a>
    </form>
@endsection
