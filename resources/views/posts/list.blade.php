@extends('layouts.master')

@section('title')
    <title>Danh sách post</title>
@endsection
@section('content')
    @if (Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
        </div>
    @endif
    <table class="table">
        <thead>
        <tr>
            <th scope="col">STT</th>
            <th scope="col">Tiêu đề</th>
            <th scope="col">Mô tả</th>
            <th scope="col">Ảnh</th>
            <th scope="col">Người viết</th>
            <th scope="col">Thời gian viết</th>
            @role('admin')
            <th scope="col"><a href="{{ route('posts.create') }}" class="btn btn-success">Thêm</a></th>
            @endrole

        </tr>
        </thead>
        <tbody>
        @foreach($posts as $i => $post)
            <tr>
                <th scope="row">{{$i +1 }}</th>
                <td>{{$post->title}}</td>
                <td>{{$post->description}}</td>
                <td><img style="width:150px; height:80px" src="{{$post->images}}" alt="ảnh"></a>
                </td>
                <td>{{ optional($post->user)->name }}</td>
                <td>{{$post->created_at}}</td>
                @role('admin')
                <td>
                    <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-warning">Edit</a>
                    <button type="button" class="btn btn-danger bg-danger" onclick="deletePost({{$post->id}})">Delete</button>

                </td>
                @endrole
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
