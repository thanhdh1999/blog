import './bootstrap';
import $ from 'jquery';
import Alpine from 'alpinejs';
import Swal from 'sweetalert2';
window.Alpine = Alpine;

Alpine.start();
// upload ảnh
import Dropzone from "dropzone";
const metaToken = document.querySelector('meta[name="csrf-token"]');
const elUpload = document.querySelector('#posts_image_upload')
if(elUpload){
let myDropzone = new Dropzone("#posts_image_upload", {
    url: '/media/upload',
    headers: {
        'X-CSRF-Token': metaToken.getAttribute('content')
    }
});
    myDropzone.on('complete', (file) => {

    })
}

// xóa bài viết
window.deletePost = (id) => {
    Swal.fire({
        icon: 'question',
        title: 'Delete',
        text:'Bạn có chắc chắn xóa',
        showConfirmButton: true,
        showCancelButton: true
    }).then(res => {
        if (res.isConfirmed) {
            axios.delete('/posts/destroy/' + id)
                .then(apiRes => {
                    Swal.fire({
                        icon: 'success',
                        title: 'Deleted',
                        text: ' Deleted successfully!',
                        showConfirmButton: true
                    }).then(() => {
                        location.reload();
                    });
                })
                .catch(err => {
                    console.error(err);
                });
        }
    });
}
//ẩn hiện comment
window.activeComment = (id) => {
    console.log(id)
    Swal.fire({
        title: ' Đổi trạng thái ',
        text: "Thay đổi trạng thái comment?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'OK',
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '/comments/update/' + id,
                type: 'POST',
                data: {
                    _token: $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    Swal.fire(
                        'Thành công',
                        'Đã thay đổi trạng thái comment',
                        'success'
                    ).then(() => {
                        window.location.reload();
                    });
                },
                error: function (xhr) {
                    Swal.fire(
                        'Error!',
                        'Không thể cập nhật trạng thái',
                        'error'
                    );
                }
            });
        }
    });
}
