<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'content' => 'required',
            'description' => 'required',
//            'images' => 'required',
            'comment_id' => 'nullable',
            'status' => ['required', 'numeric', 'check_status'],
        ];
    }
    public function messages()
    {
        return [
            'title.required' => 'Vui lòng nhập tiêu đề.',
            'content.required' => 'Vui lòng nhập nội dung.',
            'description.required' => 'Vui lòng nhập mô tả.',
//            'image.required' => 'Vui lòng chọn ảnh.',
//            'image.image' => 'Tệp tải lên phải là hình ảnh.',
            'status.required' => 'Vui lòng nhập trạng thái.',
            'status.numeric' => 'Trạng thái phải là một số.',
            'status.check_status' => 'Trạng thái chỉ được nhập 0 hoặc 1.',
            ];
    }
}
