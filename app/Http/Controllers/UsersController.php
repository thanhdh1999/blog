<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Session;
class UsersController extends Controller
{
    public function index(){
//        $role = Role::create(['name' => 'admin']);// tạo quyền mới trong bảng roles(name là key, admin là tên quyền)
//        $permission = Permission::create(['name' => 'edit']); //thêm thao tác quyền trong bảng permission ; edit là thao tác)
//        $role = Role::find(52); // tìm id của role(quyền admin or user or quyền  j đó trong bảng role);(sau sẽ truyền vào $id) từ cái check box khi nhấn vào check
//        //box nào thì sẽ lấy id của cái role đó gán vào đây ví dụ quyền admin, quyền user, quyền giảng viên, quyền bạn đọc...
//        $permission = Permission::find(3);// tìm thao tác trong bảng permission;(sau sẽ truyền vào $id) từ cái check box khi nhấn vào check
//        //box nào thì sẽ lấy id của cái thao tác đó gán vào đây
//        $role->givePermissionTo($permission);// thực hiện gán cho nó có thể thao tác gì trong bảng permissions(vd 3 kia là quyền edit)
//        $permission->assignRole($role);// ngược lại với cái trên thôi; tên quyền gán ngược lại cho vai trò role
//        $permission->syncRoles($role);//chưa có quyền thì dùng assignRole nhưng khi có rồi lại gán tiếp cho nó sẽ sai; lúc đấy phải dùng
//        //syncRoles để xem nếu nó được gán vai trò rồi nó vẫn gán lại mà nó ko bị sai
//        $role->revokePermissionTo($permission); //xóa quyền thao tác đó khỏi role đó
//        $permission->removeRole($role);// xóa role đó khỏi quyền thao tác đó
//        auth()->user()->assignRole(['admin','user']);//cấp quyền user đang login thành quyền admin
//        auth()->user()->syncRoles(['admin']);//đồng bộ quyền
        $user= User::find(21);
        $user->givePermissionTo(['delete']);//gán quyền qua model; cấp 1 lần thì dùng givePermissionTo(có thể cấp nhiều quyền<mảng quyền>)
        // khi đã cấp rồi mà đồng bộ lại thì dùng syncPermissions; vd $user->syncPermissions(['delete','add']) đồng bộ lại chỉ giữ lại delete và quyền add; các quyền khác bay hết
        //        $permission->removeRole('admin'); //xóa quyền đây dùng removeRole
//        dd(auth()->user());
//        auth()->user()->assignRole(['delete']);
//        auth()->user()->assignRole(['user']);
//        auth()->user()->syncRoles(['admin']);//cấp quyền user đang login thành quyền admin
//        if(  auth()->user()->hasRole(['user'])){//hasRole check 1 quyền trong mảng; nếu là chuỗi quyền thì dùng hasAnyRole
//            echo "có quyền trên";
//        }else{
//            echo"ko có quyền trên";
//        }

        $users = User::all();
        return view('users.list',compact('users'));
    }
    public function permission(){
        return view('users.permission');

    }
    public function create(){

    }
    public function store(){

    }
    public function edit(){

    }
    public function update(){

    }
    public function destroy(){

    }
}
