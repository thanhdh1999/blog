<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function mediaUpload(Request $request){
        $file = $request->file('file');
        //xử lý file
        $path = $file->store('media','public');
            $image = [
            'disk'=>"public",
            'path'=>$path
        ];
       session()->put('media',$image);
        return response()->json($image,200);

    }

}
