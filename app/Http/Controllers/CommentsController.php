<?php

namespace App\Http\Controllers;

use App\Models\CommentsModel;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    public function index(){
        $comments = CommentsModel::all();
        return view('comments.list',compact('comments'));
    }
    public function update($id){
        $comment = CommentsModel::find($id);
        try{
            if($comment->status == 0){
                $comment->update([
                    'status' => 1
                ]);
            }else{
                $comment->update([
                    'status' => 0
                ]);
            }
            return redirect()->route('comments.index')->with('success', ' Thành công');
        }catch (\Exception $e){
            return redirect()->back()->with('error', 'Thất bại');
        }
    }
}
