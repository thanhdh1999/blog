<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostsRequest;
use Illuminate\Http\Request;
use App\Models\PostsModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class PostsController extends Controller
{
    public function index(){
        $posts = PostsModel::paginate(30);
        $posts->getcollection()->transform(function($item){
            if (isset($item->images['disk']) && isset($item->images['path']))
                $item->images = Storage::disk($item->images['disk'])->url($item->images['path']);
             else
                $item->images = '/storage/noimage.jpg';

            return $item;
        });
        return view('posts.list', compact('posts'));
    }
    public function create(){
        return view ('posts.create');
    }
    public function store(PostsRequest $request)
    {
        $data = $request->all();
        $data['user_id']=Auth::user()->id;
        $media = session('media');
        $data['images'] = $media;
        PostsModel::create($data);
        return redirect()->route('posts.index')->with('success', 'Thêm thành công');
    }

    public function edit($id){
        $posts = PostsModel::findOrFail($id);
        return view('posts.edit',compact('posts'));

    }
    public function update(PostsRequest $request, $id)
    {
        $post = PostsModel::find($id);
        if (!$post) {
            return redirect()->route('posts.index')->with('error', 'Không tìm thấy bài viết');
        }

        if ($post->user_id != Auth::user()->id) {
            return redirect()->route('posts.index')->with('error', 'Bạn không được phép sửa ');
        }

        $data = $request->all();
        $data['images'] = session('media');
        $post->update($data);
        return redirect()->route('posts.index')->with('success', 'Cập nhật bài viết thành công');
    }

    public function destroy($id){
        $post = PostsModel::find($id);
        if($post->delete())
           return response()->json('Xóa thành công',200);

        return response()->json('Xóa thất bại',401);
    }
}
