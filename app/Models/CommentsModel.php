<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CommentsModel extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'comments';
    protected $fillable = [
        'content',
        'user_id',
        'post_id',
    ];
    public function post(){
        return $this->belongsTo(PostsModel::class);
    }
    public  function user(){
        return $this->belongsTo(User::class);
    }

}
