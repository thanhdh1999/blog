<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostsModel extends Model
{
    use HasFactory;
    protected $table = 'posts';
    public $fillable = [
        'title',
        'content',
        'description',
        'images',
        'comment_id',
        'user_id',
    ];
    protected $casts=[
        'images'=>'json'
    ];
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function comment(){
        return $this->hasMany(CommentsModel::class);
    }
}
